# Tasks

Tasks is a small REST app for dealing with users everyday tasks.

### Prerequisites

```
PHP > 7.0
composer
docker
```

### Installing

1. Install packages
    ```
    composer install 
    ```
2. change db params (db_user, db_password, db_name) in file .env
    ```
    DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.6"
    ```
3. start docker db container 
    ```
    docker-compose up
    ```
4. start db migration command 
    ```
    php bin\console doctrine:migrations:migrate
    ```
5. generate security keys for JWT tokens
    ```
    php bin/console lexik:jwt:generate-keypair
    ```
    -for Win users (start command in Git bash or similar): 
    ```
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout config/jwt/private.pem -out config/jwt/public.pem
    ```
5.  start local symfony server 
    ```
    symfony server:start
    ```
## Description
App is built with the use of Symfony framework and API Platform bundle. API authorization is JWT type, app is using [JWT Bundle](https://github.com/lexik/LexikJWTAuthenticationBundle).

API Platform bundle comes with a handy swagger support. Swagger docs can be found on endpoint ```^api/docs```

App supports: pagination, filtering tasks by name and by date (every other filter can be easily added).


## Improvements
If the app is extremely popular and has a huge number of visitors we can speed up response with: 

- Saving data in db can be done asynchronous (Rabbitmq)

- Return data can be cached (Redis) 

Also we can put PHP code and web server in docker containers.

Custom filter/flag  ```today-tasks``` with flag (true|false).

## User flow
Proposed User Story: "As a user, I want to have an ability to see a list of tasks for my day, so that I can do them one by one".

App supports two types of authenticated users: admin and regular, with different permission levels.

Anonymous users can register (create new user).

Regular user can CRUD his data, also he can CRUD on their owned tasks.

Admin users are able to CRUD all records and users.

## Test
Functionals test covers most of the API requests, you can start them with command:
```
php bin/phpunit
```

## Built With

* [Symfony](https://symfony.com/) - The web framework used
* [API Platform](https://api-platform.com/) - The API Platform Framework
* [Docker](https://www.docker.com/) - OS-level virtualization to deliver software in containers
* [Swagger](https://swagger.io/) - Swagger is an Interface Description Language for describing RESTful APIs expressed using JSON. 