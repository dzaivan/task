<?php
/**
 * Created by PhpStorm.
 * Date: 3/11/2021
 * Time: 12:37 AM
 */

namespace App\Test;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Base class for testing API requests
 * TODO instead of this class we can can use data fixtures
 * Class CustomApiTestCase
 * @package App\Test
 */
class CustomApiTestCase extends ApiTestCase
{
    private $token;
    protected $client;

    protected $usersData =  [
        ['email' => 'test@example.com', 'password'  => 'test'],
        ['email' => 'test1@example.com', 'password'  => 'test'],
        ['email' => 'test@2example.com', 'password'  => 'test', 'isAdmin' => true]
    ];

    protected $users = [];
    protected $user = null;
    protected $tasks = [];
    protected $em = null;

    public function setUp(): void
    {
        self::bootKernel();
    }

    public function createUser(string $email, string $password, $isAdmin = false): User
    {
        $user = new User();
        $user->setEmail($email);
        $user->setFullName(substr($email, 0, strpos($email, '@')));
        $encoded = self::$container->get('security.password_encoder')
            ->encodePassword($user, $password);
        $user->setPassword($encoded);
        if ($isAdmin) {
            $user->setRoles(["ROLE_ADMIN"]);
        }
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * Login user with given credentials, set token and user property
     * @param string $email
     * @param string $password
     * @param Client $client
     * @return Client
     */
    protected function login(string $email, string $password, Client $client): Client
    {
        $this->client = $client;
        $this->getToken($email, $password);
        $this->setClientWithToken();
        return $this->client;
    }

    protected function setClientWithToken($token = null): void
    {
        $token = $token ?: $this->getToken();
        $this->client->setDefaultOptions(
            ['headers' =>
                [
                    'authorization'    => 'Bearer ' . $token,
                    'Content-Type'     => 'application/json',
                    'Accept'           => 'application/json'
                ]
            ]);
    }

    /**
     * Sends POST API request to server and on success returns token
     * @param string $email
     * @param string $password
     * @return string  - token after login
     */
    protected function getToken(string $email = "", string $password = ""): string
    {
        if ($this->token) {
            return $this->token;
        }

        $response = $this->client->request('POST', '/authentication_token', [
            'json' => [
                'email' => $email,
                'password' => $password,
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;
        return $this->token;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        if ($this->em == null) {
            $this->em = self::$container->get('doctrine')->getManager();
        }
        return $this->em;
    }

    /**
     * Creates three users for test purposes, third user is with admin role
     * TODO This needs to be build with the help of fixtures
     *
     * @param Client $client
     * @param int $loginUserNumber - 0, 1 are regular users, user 2 is admin
     */
    protected function createThreeUsersAndLogin(Client $client, $loginUserNumber = 0): void
    {
        foreach ($this->usersData as $userData) {
            $isAdmin = isset($userData['isAdmin']) ? $userData['isAdmin'] : false;
            $this->users[] = $this->createUser($userData['email'], $userData['password'], $isAdmin);
        }
        $this->loginTestUser($client, $loginUserNumber);
    }

    /**
     * Authorize one of the test users according to param $loginUserNumber
     * @param Client $client
     * @param integer $loginUserNumber
     */
    protected function loginTestUser(Client $client, int $loginUserNumber): void
    {
        $this->token = null; //reset token
        $loginUserData = $this->usersData[$loginUserNumber];
        $this->login($loginUserData['email'], $loginUserData['password'], $client);
        $this->user = $this->users[$loginUserNumber];
    }

    protected function createTask($properties, $user)
    {
        $task = new Task();
        $task->setName($properties['name']);
        $task->setDescription($properties['description']);
        $task->setDate($properties['date']);
        $task->setOwner($user);
        $em = $this->getEntityManager();
        $em->persist($task);
        $em->flush();

        return $user;
    }

    /**
     * Creates 5 tasks for given user
     * TODO This needs to be build with the help of fixtures
     * @param $user
     */
    protected function createFiveTasksForTheUser($user): void
    {
        $todayDate      = new \DateTime();
        $yesterdayDate  = new \DateTime('-1 day');
        $tomorrowDate   = new \DateTime('+1 day');
        $tasksData =  [
            ['name' => 'Task1', 'description'  => 'Desc1', 'date' => $todayDate],
            ['name' => 'Task2', 'description'  => 'Desc2', 'date' => $yesterdayDate],
            ['name' => 'Task3', 'description'  => 'Desc3', 'date' => $todayDate],
            ['name' => 'Task4', 'description'  => 'Desc4', 'date' => $tomorrowDate],
            ['name' => 'Task5', 'description'  => 'Desc5', 'date' => $tomorrowDate],
        ];

        foreach ($tasksData as $taskData) {
            $this->tasks[$user->getId()][] = $this->createTask($taskData, $user);
        }
    }

    /**
     * Reload entity user after every client request
     */
    protected function reloadUserAfterRequest(): void
    {
        $em = $this->getEntityManager();
        $this->user = $em->getRepository(User::class)->find($this->user->getId());
    }

}