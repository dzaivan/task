<?php
/**
 * Created by PhpStorm.
 * User: zorica
 * Date: 3/10/2021
 * Time: 7:32 PM
 */

namespace App\Serializer;
use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * This class dynamically create and adds specific groups (admin:read | admin:write)
 * for normalization/denormalization context according to user roles
 * @package App\Serializer
 */
final class AdminGroupsContextBuilder implements SerializerContextBuilderInterface
{
    private $decorated;
    private $authorizationChecker;
    public function __construct(SerializerContextBuilderInterface $decorated, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->decorated = $decorated;
        $this->authorizationChecker = $authorizationChecker;
    }
    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
//        $resourceClass = $context['resource_class'] ?? null;

        $isAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN');
        if (isset($context['groups']) && $isAdmin) {
            $context['groups'][] = $normalization ? 'admin:read' : 'admin:write';
        }

        return $context;
    }
}