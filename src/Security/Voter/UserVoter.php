<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

/**
 * User voter for deciding who can manage user object (access control per object)
 * Class UserVoter
 * @package App\Security\Voter
 */
class UserVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {

        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['MANAGE'])
            && $subject instanceof User;

    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'MANAGE':
                if ($user->getId() === $subject->getId()) {
                    return true;
                }

                if ($this->security->isGranted("ROLE_ADMIN")) {
                    return true;
                }

        }
        return false;
    }
}
