<?php
/**
 * Created by PhpStorm.
 * Date: 3/10/2021
 * Time: 8:56 PM
 */

namespace App\Tests\Functional;

use App\Entity\User;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

/**
 * Class UserTest
 * Test class for checking API CRUD requests to User Entity
 * @package App\Tests\Functional
 */
class UserTest extends CustomApiTestCase
{
    // This AliceBundle trait will empty the database content before every test call
    use ReloadDatabaseTrait;

    /**
     * Test access of anonymous user for all requests:
     * ** collection request (get, post) and item requests (put, patch, delete, get) **
     *  Access control:
     *  - Anonymous users can access to POST user collection (anonymous users can create user)
     *  - Anonymous users can't access to (PUT, PATCH, DELETE, GET, GET-collections) user API requests
     */
    public function testAccessUsersForAnonymousUser()
    {
        $client = self::createClient(); //anonymus user

        $email = "test@a.com";
        $password = "test";

        $user = $this->createUser($email, $password);

        $em = $this->getEntityManager();
        $users = $em->getRepository(User::class)->findAll();
        $this->assertEquals(1, count($users));

        $client->request('GET', '/api/users',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(401);

        /* accessable for anonymus user, should return 400 (bad request) */
        $client->request('POST', '/api/users',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(400);

        $client->request('PUT', '/api/users/' . $user->getId(),
            [
                'headers'   => [ 'Content-Type' => 'application/json' ],
                'json'      => []
            ]);
        $this->assertResponseStatusCodeSame(401);

        $client->request('PATCH', '/api/users/' . $user->getId(),
            [   'headers'   => ['Content-Type' => 'application/merge-patch+json'],
                'json'      => []
            ]);
        $this->assertResponseStatusCodeSame(401);

        $client->request('DELETE', '/api/users/' . $user->getId(),
            [   'headers'   => ['Content-Type' => 'application/json'],
                'json'      => []
            ]);
        $this->assertResponseStatusCodeSame(401);
    }

    /**
     * Test creation of the user
     * ** collection POST request **
     * Access control:
     * - Anonymous users can access to POST user collection (anonymous users can create user)
     */
    public function testCreateUser()
    {
        /** create user - invalid request data **/
        $client = self::createClient(); //anonymus user
        $client->request('POST', '/api/users', [
            'json' => [
                'email' => 'test@example.com',
                'password' => 'test'
            ]
        ]);
        $this->assertResponseStatusCodeSame(422); //fullName property is missing

        /** create user (valid data) **/
        $client->request('POST', '/api/users', [
            'json' => [
                'email'     => 'test@example.com',
                'password'  => 'test',
                'fullName' => 'test_fullName'
            ]
        ]);
        $this->assertResponseStatusCodeSame(201); //resource created

        /** try to create user and set as admin - not allowed **/
        $response = $client->request('POST', '/api/users', [
            'json' => [
                'email'     => 'test1@example.com',
                'password'  => 'test',
                'fullName'  => 'test_fullName',
                'roles'     => ["ROLE_ADMIN"]
            ]
        ]);
        $this->assertResponseStatusCodeSame(201); //resource created
        $data = $response->toArray();

        $this->assertEquals($data['roles'],["ROLE_USER"]); //app automatically set role to regular user
    }

    /**
     * Test creation and logging of newly created user
     * ** GET request **
     * Access control:
     * - Regular users can see only one user in GET collection response
     */
    public function testCreateLoginUser()
    {
        $client = self::createClient();

        /** create user and login, request "API GET users" returns only one user (just that user) */
        $email = "test@a.com";
        $password = "test";

        $this->createUser($email, $password);
        $this->login($email, $password, $client);

        $response = $this->client->request('GET', '/api/users',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(200);
        $data = $response->toArray();
        $this->assertEquals(count($data), 1);
    }

    /**
     * Test creation of 3 users, test GET collection for regular user and for admin user
     * ** GET request **
     * Access control:
     * - Regular users can see only one user in GET collection response
     * - Admin users can see all users
     */
    public function testGetUserCollectionRequest()
    {
        $client = self::createClient();

        $this->createThreeUsersAndLogin($client, 0);

        /** Login as regular user, GET collection response has only one item (that user) */
        $response = $this->client->request('GET', '/api/users',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(200);
        $data = $response->toArray();
        $this->assertEquals(1, count($data));

        /** login as admin user, in response collection there should be all items/users */
        $this->loginTestUser($this->client, 2);
        $response = $this->client->request('GET', '/api/users',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(200);
        $data = $response->toArray();
        $this->assertEquals(3, count($data));
    }

    /**
     * Test PUT request for regular users
     * ** PUT request **
     * Access control:
     * - Regular user can change his params
     * - Regular user can't change user params for other users
     * - Regular user can't update "roles" params (can't promote self to admin role)
     */
    public function testPutUserRequest()
    {
        $newFullName = "somethingDifferent";
        $client = self::createClient();

        $this->createThreeUsersAndLogin($client, 0);

        /** check PUT request, regular user can change his params */
        $this->client->request('PUT', '/api/users/' . $this->user->getId(),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(200);

        $updatedUser = $this->getEntityManager()->getRepository(User::class)->find($this->user->getId());
        $this->assertEquals($updatedUser->getFullName(), $newFullName);

        /** check PUT request, regular user can't change user params for other users id+1 */
        $this->client->request('PUT', '/api/users/' . ($this->user->getId() + 1),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(403);

        /** check PUT request, regular user can't update roles params */
        $response = $this->client->request('PUT', '/api/users/' . ($this->user->getId()),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => ['roles' => ["ROLE_ADMIN"]]
            ]);
        $data = $response->toArray();
        $this->assertEquals($data['roles'], ["ROLE_USER"]); //app automatically set role to regular user
    }

    /**
     * Test PUT request for admin users
     * ** PUT request **
     * Access control:
     * - Admin user can change his params
     * - Admin user can change params (properties) for other users
     * - Admin user can update "roles" params (can promote other users to admin role)
     */
    public function testPutUserRequestForAdminUser()
    {
        $newFullName = "somethingDifferent";
        $client = self::createClient();

        $this->createThreeUsersAndLogin($client, 2);

        /** check PUT request, admin user can change his params */
        $this->client->request('PUT', '/api/users/' . $this->user->getId(),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(200);

        $updatedUser = $this->getEntityManager()->getRepository(User::class)->find($this->user->getId());
        $this->assertEquals($updatedUser->getFullName(), $newFullName);

        /** check PUT request, admin user can change user params for others id+1 */
        $this->client->request('PUT', '/api/users/' . ($this->user->getId() -1),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(200);

        /** check PUT request, regular user can update roles params */
        $response = $this->client->request('PUT', '/api/users/' . ($this->user->getId() - 1),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => ['roles' => ["ROLE_ADMIN"]]
            ]);
        $data = $response->toArray();
        $this->assertTrue(in_array("ROLE_ADMIN", $data['roles'])); //admin can update users role
    }

    /**
     * Test PATCH request for regular users
     * ** PATCH request **
     * Access control:
     * - Regular user can change his params
     * - Regular user can't change params (properties) for other users
     * - Regular user can't update "roles" params (can't promote self to admin role)
     */
    public function testPatchUserRequest()
    {
        $newFullName = "somethingDifferent";
        $client = self::createClient();

        $this->createThreeUsersAndLogin($client, 0);

        /** check PATCH request, regular user can change his params */
        $this->client->request('PATCH', '/api/users/' . $this->user->getId(),
            [
                'headers' => ['Content-Type' => 'application/merge-patch+json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(200);

        $updatedUser = $this->getEntityManager()->getRepository(User::class)->find($this->user->getId());
        $this->assertEquals($updatedUser->getFullName(), $newFullName);

        /** check PATCH request, regular user can't change user params for other users */
        $this->client->request('PATCH', '/api/users/' . ($this->user->getId() + 1),
            [
                'headers' => ['Content-Type' => 'application/merge-patch+json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(403);

        /** check PATCH request, regular user can't update roles params */
        $response = $this->client->request('PATCH', '/api/users/' . ($this->user->getId()),
            [
                'headers' => ['Content-Type' => 'application/merge-patch+json'],
                'json' => ['roles' => ["ROLE_ADMIN"]]
            ]);
        $data = $response->toArray();
        $this->assertEquals($data['roles'], ["ROLE_USER"]); //app automatically set role to regular user
    }

    /**
     * Test PATCH request for admin users
     * ** PATCH request **
     * Access control:
     * - Admin user can change his params
     * - Admin user can change params (properties) for other users
     * - Admin user can update "roles" param (can promote other users to admin role)
     */
    public function testPatchUserRequestForAdminUser()
    {
        $newFullName = "somethingDifferent";
        $client = self::createClient();

        $this->createThreeUsersAndLogin($client, 2);

        /** check PUT request, admin user can change his params */
        $this->client->request('PATCH', '/api/users/' . $this->user->getId(),
            [
                'headers' => ['Content-Type' => 'application/merge-patch+json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(200);

        $updatedUser = $this->getEntityManager()->getRepository(User::class)->find($this->user->getId());
        $this->assertEquals($updatedUser->getFullName(), $newFullName);

        /** check PUT request, admin user can change user params for others id+1 */
        $this->client->request('PATCH', '/api/users/' . ($this->user->getId() -1),
            [
                'headers' => ['Content-Type' => 'application/merge-patch+json'],
                'json' => ['fullName' => $newFullName]
            ]);
        $this->assertResponseStatusCodeSame(200);

        /** check PUT request, regular user can update roles params */
        $response = $this->client->request('PATCH', '/api/users/' . ($this->user->getId() - 1),
            [
                'headers' => ['Content-Type' => 'application/merge-patch+json'],
                'json' => ['roles' => ["ROLE_ADMIN"]]
            ]);
        $data = $response->toArray();
        $this->assertTrue(in_array("ROLE_ADMIN", $data['roles'])); //admin can update users role
    }

    /**
     * Test DELETE request for regular users
     * ** DELETE request **
     * Access control:
     * - Regular user can't delete other users
     * - Regular user can delete his account
     */
    public function testDeleteUserRequest()
    {
        $client = self::createClient();

        $this->createThreeUsersAndLogin($client, 0);

        /** check DELETE user, regular user can't delete other users */
        $this->client->request('DELETE', '/api/users/' . ($this->user->getId() + 1),
            [
                'headers' => ['Content-Type' => 'application/json']
            ]);
        $this->assertResponseStatusCodeSame(403);

        /** check DELETE user, regular user can delete his account */
        $this->client->request('DELETE', '/api/users/' . $this->user->getId(),
            [
                'headers' => ['Content-Type' => 'application/json']
            ]);
        $this->assertResponseStatusCodeSame(204);
    }

    /**
     * Test DELETE request for admin users
     * ** DELETE request **
     * Access control:
     * - Admin user can delete other users
     * - Admin user can delete his account
     */
    public function testDeleteUserForAdminUsersRequest()
    {
        $client = self::createClient();

        $this->createThreeUsersAndLogin($client, 2);

        /** check DELETE user, admin user can delete other users */
        $this->client->request('DELETE', '/api/users/' . ($this->user->getId() - 1),
            [
                'headers' => ['Content-Type' => 'application/json']
            ]);
        $this->assertResponseStatusCodeSame(204);

        /** check DELETE user, admin user can delete his account */
        $this->client->request('DELETE', '/api/users/' . $this->user->getId(),
            [
                'headers' => ['Content-Type' => 'application/json']
            ]);
        $this->assertResponseStatusCodeSame(204);
    }
}