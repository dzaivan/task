<?php
/**
 * Created by PhpStorm.
 * Date: 3/11/2021
 * Time: 6:43 PM
 */

namespace App\Tests\Functional;

use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

/**
 * Class TaskTest
 * Test class for checking API CRUD requests to Task Entity
 * @package App\Tests\Functional
 */
class TaskTest extends CustomApiTestCase
{
    // This AliceBundle trait will empty the database content before every test call
    use ReloadDatabaseTrait;

    /**
     * Test access of anonymous user for all requests:
     * ** collection request (get, post) and item requests (put, patch, delete, get) **
     *  Access control:
     *  - Anonymous users can't access to (PUT, PATCH, DELETE, GET, GET-collections, POST) tasks API requests
     */
    public function testAccesTasksForAnonymousUser()
    {
        $client = self::createClient(); //anonymus user

        $client->request('GET', '/api/tasks',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(401);


        $client->request('POST', '/api/tasks',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(401);

        $client->request('PUT', '/api/tasks/3',
            [
                'headers'   => [ 'Content-Type' => 'application/json' ],
                'json'      => []
            ]);
        $this->assertResponseStatusCodeSame(401);

        $client->request('PATCH', '/api/tasks/4',
            [   'headers'   => ['Content-Type' => 'application/merge-patch+json'],
                'json'      => []
            ]);
        $this->assertResponseStatusCodeSame(401);

        $client->request('DELETE', '/api/tasks/6',
            [   'headers'   => ['Content-Type' => 'application/json'],
                'json'      => []
            ]);
        $this->assertResponseStatusCodeSame(401);
    }

    /**
     * Test creation of the task for fully authenticated user
     * ** collection POST request **
     */
    public function testCreateTask()
    {
        $client = self::createClient();

        /** create user and login, create task for the user */
        $email = "test@a.com";
        $password = "test";

        $user = $this->createUser($email, $password);
        $this->login($email, $password, $client);

        /** create task for the user */
        $this->client->request('POST', '/api/tasks',
            ['headers' => ['Content-Type' => 'application/json'],
                'json' => [
                    "name" => "taskName",
                    "description" => "Task Desc",
                    "owner" => "api/users/" . $user->getId(),
                    "date" => "2021-03-11"
                ]
            ]);
        $this->assertResponseStatusCodeSame(201);


        /** create task with invalid owner */
        $this->client->request('POST', '/api/tasks',
            ['headers' => ['Content-Type' => 'application/json'],
                'json' => [
                    "name" => "taskName",
                    "description" => "Task Desc",
                    "owner" => "api/users/" . ($user->getId() + 234),
                    "date" => "2021-03-11"
                ]
            ]);
        $this->assertResponseStatusCodeSame(400);
    }

    /**
     *  Test GET tasks for fully authenticated user
     *  ** collection GET request **
     *  Access control:
     *  - Regular users can see only their owned tasks
     *  - Admin users can see all tasks
     */
    public function testGetTasks()
    {
        $client = self::createClient();

        /** testing "GET tasks" -> users can see only their owned tasks */
        $this->createThreeUsersAndLogin($client, 0);
        $this->createFiveTasksForTheUser($this->user);

        /** log as other user and create 5 task for this user also */
        $this->loginTestUser($this->client, 1);
        $this->reloadUserAfterRequest();
        $this->createFiveTasksForTheUser($this->user);

        $response = $this->client->request('GET', '/api/tasks',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(200);

        $data = $response->toArray();
        $this->assertEquals(5, count($data)); //user can see only 5 tasks

        /** GET request tasks -> admin users can see all created tasks */
        $this->loginTestUser($this->client, 2);
        $this->reloadUserAfterRequest();
        $this->createFiveTasksForTheUser($this->user);

        $response = $this->client->request('GET', '/api/tasks',
            ['headers' => ['Content-Type' => 'application/json']]);
        $this->assertResponseStatusCodeSame(200);

        $data = $response->toArray();
        $this->assertEquals(15, count($data)); // 3 users created 5 task = 15 tasks
    }

    /**
     *  Test GET todays tasks for fully authenticated user
     *  ** collection GET request **
     *  Access control:
     *  - Regular users can see only their owned tasks
     *  - Admin users can see all tasks
     */
    public function testGetTodaysTask()
    {
        $client = self::createClient();

        /** testing "GET tasks" -> filter today's tasks */
        $this->createThreeUsersAndLogin($client, 0);
        $this->createFiveTasksForTheUser($this->user); //create 5 tasks, 1 for yesterday, 2 for today, 2 for tomorrow

        $yesterday = new \DateTime('-1 day');
        $tomorrow = new \DateTime('+1 day');

        $response = $this->client->request('GET', '/api/tasks',
            [
                'headers' => ['Content-Type' => 'application/json'],
                'query'     => [
                    'date[strictly_before]' => $tomorrow->format("Y-m-d"),
                    'date[strictly_after]'  => $yesterday->format("Y-m-d"),
                ]
            ]);
        $this->assertResponseStatusCodeSame(200);
        $data = $response->toArray();
        $this->assertEquals(2, count($data)); // 2 today's tasks

        /** testing "GET tasks" for admin user -> filter today's tasks for all users */
        $this->loginTestUser($this->client, 2);
        $this->reloadUserAfterRequest();
        $this->createFiveTasksForTheUser($this->user); //create 5 tasks, 1 for yesterday, 2 for today, 2 for tomorrow

        $response = $this->client->request('GET', '/api/tasks',
            [
                'headers' => ['Content-Type' => 'application/json'],
                'query'     => [
                    'date[strictly_before]' => $tomorrow->format("Y-m-d"),
                    'date[strictly_after]'  => $yesterday->format("Y-m-d"),
                ]
            ]);
        $this->assertResponseStatusCodeSame(200);
        $data = $response->toArray();
        $this->assertEquals(4, count($data)); // 4 today's tasks
    }

}